 <html>
 
 <head>
 
	<title>CG</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php
		include "html/references.php";
	?>
	<style>
		body{
			font: 400 15px/1.8 Lato, sans-serif;
			color: #777;
		}
		h3, h4 {
			margin: 10px 0 30px 0;
			letter-spacing: 10px;
			font-size: 20px;
			color: #111;
		}
		h5	{
			margin: 10px 0 30px 0;
			font-size: 20px;
			color: #111;
		}
		h6	{
			margin: 10px 0 30px 0;
			letter-spacing: 6px;
			font-size: 70px;
			color:white;
			font-family:"28 Days Later";
		}
		.container {
			padding: 80px 120px;
		}
		.person {
			border: 10px solid transparent;
			margin-bottom: 25px;
			width: 80%;
			height: 80%;
			opacity: 0.9;
		}
		.person:hover {
			border-color: #f1f1f1;
		}
		.carousel-inner img {
			margin: auto;
		}
		.carousel-caption h3 {
			color: #fff !important;
		}
		@media (max-width: 600px) {
			.carousel-caption {
				display: none; /* Hide the carousel text when the screen is less than 600 pixels wide */
			}
		}
		.bg-1 {
			background: #2d2d30;
			color: #bdbdbd;
		}
		.bg-1 h3 {color: #fff;}
		.bg-1 p {font-style: italic;}
		.list-group-item:first-child {
			border-top-right-radius: 0;
			border-top-left-radius: 0;
		}
		.list-group-item:last-child {
			border-bottom-right-radius: 0;
			border-bottom-left-radius: 0;
		}
		.thumbnail {
			padding: 0 0 15px 0;
			border: none;
			border-radius: 0;
		}
		.thumbnail p {
			margin-top: 15px;
			color: #555;
		}
		.btn {
			padding: 10px 20px;
			background-color: #333;
			color: #f1f1f1;
			border-radius: 0;
			transition: .2s;
		}
		.btn:hover, .btn:focus {
			border: 1px solid #333;
			background-color: #fff;
			color: #000;
		}
		.modal-header, h4, .close {
			background-color: #333;
			color: #fff !important;
			text-align: center;
			font-size: 30px;
		}
		.modal-header, .modal-body {
			padding: 40px 50px;
		}
		.nav-tabs li a {
			color: #777;
		}
		.navbar {
			font-family: Montserrat, sans-serif;
			margin-bottom: 0;
			background-color: #2d2d30;
			border: 0;
			font-size: 11px !important;
			letter-spacing: 4px;
			opacity: 0.9;
		}
		.navbar li a, .navbar .navbar-brand {
			color: #d5d5d5 !important;
		}
		.navbar-nav li a:hover {
			color: #fff !important;
		}
		.navbar-nav li.active a {
			color: #fff !important;
			background-color: #29292c !important;
		}
		.navbar-default .navbar-toggle {
			border-color: transparent;
		}
		.open .dropdown-toggle {
			color: #fff;
			background-color: #555 !important;
		}
		.form-control {
			border-radius: 0;
		}
		
	</style>
 </head>
 
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="50">
	
	<?php 
		include "../the-cheat-game/html/home-header.php";
		include "../the-cheat-game/html/carousel.php";
		include "../the-cheat-game/html/home.php";
	?>
</body>
<?php include "../the-cheat-game/html/animatescroll.php" ?>
</html> 