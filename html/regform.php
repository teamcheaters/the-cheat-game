
	<div class="container-fluid text-center" >
		<div class="row" id = "login-form">
			<div class="center">
				<table align="center" style="width:30%; border: 2px solid #ddd; background-color:white">
					<tr>
						<td>
							<form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" autocomplete="off">
    
							<div class="col-md-12">
							<tr>
								<td>
									<div class="form-group" align="center">
										<h2 class="">Create Account</h2>
									</div>
								</td>
							</tr>
        
							<tr>
								<td>
									<div class="form-group">
										<hr />
									</div>
								</td>
							</tr>
            
						<tr>
							<td>
						<?php
						if ( isset($errMsg) ) {
    
						?>
								<div class="form-group">
									<div class="alert alert-<?php echo ($errTyp=="success") ? "success" : $errTyp; ?>">
									<span class="glyphicon glyphicon-info-sign"></span> <?php echo $errMsg; ?>
									</div>
								</div>
							
						<?php
						}
						?>
						</td>
						</tr>
						
					<tr>
						<td>
							<div align="center" class="form-group">
								<div class="input-group">
								<input type="text" style='display: inline; width: 49%; float:left;padding:10px' name="fname" class="form-control" placeholder="First Name" maxlength="50" value="<?php echo $fname ?>" required/>
								
								<input type="text" style='display: inline; width: 49%; float:right;padding:10px' name="lname" class="form-control" placeholder="Last Name" maxlength="50" value="<?php echo $lname ?>" required/>
								
								</div>
								<span style='font-size:15px' class="text-danger"><?php echo $nameErr; ?></span>
							</div>
						</td>
					</tr>
					
						<td>
							
							<div align="center" style='font-size:15px;' class="radio">
								Gender:
								<label  class="radio-inline"><input type="radio" name="gender">Male</label>
								<label class="radio-inline"><input type="radio" name="gender">Female</label>
								<br>
								<span style='font-size:15px' class="text-danger"><?php echo $genderErr; ?></span>
							</div>
							
						</td>
					
					<tr>
						<td>
							<div class="form-group">
								<div style='margin-left:0.3cm;' class="input-group">
								<span  class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
								<input style='width: 97%;' type="email" name="email" class="form-control" placeholder="Enter Your Email" maxlength="40" value="<?php echo $email ?>" required/>
						 
								</div>
								<span style='font-size:15px' class="text-danger"><?php echo $emailErr; ?></span>
							</div>
						</td>
					</tr>
					
					
					<tr>
						<td>
							<div class="form-group">
								<div style='margin-left:0.3cm;' class="input-group">
								<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
								<input style='width: 97%;' type="text" name="uname" class="form-control" placeholder="Enter Username" maxlength="50" value="<?php echo $username ?>" />
								
							</div>
							<span style='font-size:15px' class="text-danger"><?php echo $userErr; ?></span>
						</div>
						</td>
					</tr>
					
					
					
					<tr>
						<td>
							<div class="form-group">
								<div style='margin-left:0.3cm;' class="input-group">
								<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
								<input style='width: 97%;' type="password" name="pass" class="form-control" placeholder="Enter Password" maxlength="15" required/>
								</div>
								<span style='font-size:15px' class="text-danger"><?php echo $passErr; ?></span>
							</div>
						</td>
					</tr>
					
					
					<tr>
						<td>
							<div class="form-group">
								<div style='margin-left:0.3cm;' class="input-group">
								<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
								<input style='width: 97%;' type="password" name="passmatch" class="form-control" placeholder="Confirm Password" maxlength="15" required/>
								</div>
								<span style='font-size:15px' class="text-danger"><?php echo $passErrmatch; ?></span>
							</div>
						</td>
					</tr>
				
					<tr>
						<td>
							<div class="form-group">
								<hr />
							</div>
						</td>
					</tr>
            
					<tr>
						<td>
							<div class="form-group">
								<button type="submit" class="btn btn-block btn-primary" name="btn-register">Create Account</button>
							</div>
						</td>
					</tr>
            
					
					<tr>
						<td>
							<div class="form-group">
								<a href="SignIn.php">Sign in Here</a>
							</div>
						</td>
					</tr>
            
        
						</div>
   
						</form>		
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>