<div class="container-fluid text-center" >
	<div class="row" id = "login-form">
	<div class="center">
		<table align="center" style="width:30%; border: 2px solid #ddd; background-color:white">
			<tr>
				<td>
					<form method = "post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" autocomplete="on">
		
						<div class = "col-md-12">	
						<tr>
							<td>
								<div class = "form-group" align="center">
									<h3>Sign In</h3>
								</div>
							</td>
						</tr>
			
						
							<td>
								<div class="form-group">
									<hr />
								</div>
							</td>
		<tr>
			<td>
			<?php
				if ( isset($errMsg) ) {
    
			?>
						
				<div class="form-group">
					<div class="alert alert-<?php echo ($errTyp=="success") ? "success" : $errTyp; ?>">
					<span class="glyphicon glyphicon-info-sign"></span> <?php echo $errMsg; ?>
					</div>
				</div>
							
			<?php
			}
			?>
		</td>
	</tr>
		<tr>
			<td>
				<div align="center" class="form-group">
					<div style='margin-left:0.3cm;' class = "input-group">
						<span class = "input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
						<input style='width: 98%;' type = "text" name ="uname" class="form-control" placeholder="Enter Username" value="<?php echo $username; ?>" maxlength= "50" required/>
					</div>
					<span class="text-danger"><?php echo $userErr; ?></span>
				</div>
			</td>
		</tr>
			
		<tr>
			<td>
				<div align="center" class = "form-group">	
					<div style='margin-left:0.3cm;' class = "input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
						<input style='width: 98%;' type="password" name="pass" class="form-control" placeholder="Enter Password" maxlength="15" required/>
					</div>
				</div>
			</td>
		</tr>
			
		<tr>
			<td>
				<div class="form-group">
				<hr />
				</div>
			</td>
		</tr>
			
			<td>
				<div style='margin-left:0.3cm;width: 95%;' class= "form-group">
					<button type="submit" class="btn btn-block btn-primary" name="btn-login">Sign In</button>
				</div>
			</td>
		
		
			
		<tr>
			<td>
				<div class="form-group" align="center">
					<a href="Registration.php">Sign Up Here </a>
				</div>
			</td>
		</tr>
	</div>
		
		</form>
					</td>
				</tr>
			</table>
		</div>
		
	</div>

</div>
