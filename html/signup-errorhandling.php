<?php
		include_once 'DBConnection.php';
		$error = false;
		
		$fname = isset($_POST['fname']) ? $_POST['fname'] : '';
		$lname = isset($_POST['lname']) ? $_POST['lname'] : '';
		$gender = isset($_POST['gender']) ? $_POST['gender'] : '';
		$email = isset($_POST['email']) ? $_POST['email'] : '';
		$username= isset($_POST['uname']) ? $_POST['uname'] : '';
		$password= isset($_POST['pass']) ? $_POST['pass'] : '';
		$pass1= isset($_POST['passmatch']) ? $_POST['passmatch'] : '';
		$nameErr = $genderErr = $emailErr = $userErr = "";
		$passErr = $passErrmatch ="";
		$con = mysqli_connect("localhost","root","","thecheatgame");
		
		if(isset($_POST['btn-register'])){
			
			//firstname & lastname validation
			if((strlen($fname) < 3)||(strlen($lname) < 3)){
				$error = true;
				$nameErr = "Name must have atleast 3 characters.";
			}else if(!preg_match("/^[a-zA-Z ]+$/",$fname)){
				$error = true;
				$nameErr = "Name must contain alphabets and space.";
			}else if(!preg_match("/^[a-zA-Z ]+$/",$lname)){
				$error = true;
				$nameErr = "Name must contain alphabets and space.";
			
			}
			
			//gender
			if (empty($gender)) {
				$error = true;
				$genderErr = "Gender is required.";
			} 
			
			//username validation
			if(strlen($username) < 3){
				$error = true;
				$userErr = "Username must have atleast 3 characters.";
			}else if(!preg_match("/^[a-zA-Z ]+$/",$username)){
				$error = true;
				$userErr = "Username must contain alphabets and space.";
			}else{
				$query = "SELECT Username FROM playerinfo WHERE Username = '$username'";
				$select = mysqli_query($con,$query);
				if($select->num_rows){
					$error = true;
					$userErr = "Provided Username is already in use.";
				}	
			}
			
			// email validation
			if ( !filter_var($email,FILTER_VALIDATE_EMAIL)) {
				$error = true;
				$emailErr = "Please enter valid email address.";
			}else{
				$query = "SELECT Email FROM playerinfo WHERE Email = '$email'";
				$select = mysqli_query($con,$query);
				if($select->num_rows){
					$error = true;
					$emailErr = "Provided Email is already in use.";
				}
				
			}
			
			//password validation
			if(strlen($password) < 6){
				$error = true;
				$passErr = "Password must have atleast 6 characters.";
			}
			
			//password match
			if(empty($pass1)){
				$error = true;
				$passErrmatch = "Please confirm password.";
			}else if($pass1!=$password){
				$error = true;
				$passErrmatch = "Password does not match.";
				unset($pass1);
			}
			//encrypt password
			$password = sha1($password);
			//if there's no error, continue to signup
			if(!$error){
				
				
				$query = "INSERT INTO playerinfo (Username,FirstName,LastName,Gender,Email,Password) 
				VALUES ('$username','$fname','$lname','$gender','$email','$password');";
				if($con->query($query)===TRUE){
					$errTyp = "success";
					$errMsg = "Successfully registered, you may login now";
					$username="";
					$email="";
					$fname="";
					$lname="";
					unset($password);
					unset($pass1);
				}else{
					$errTyp = "danger";
					$errMsg = "Something is wrong, try again later...";
					
				}
			}
		}
?>